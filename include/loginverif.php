<?php include_once __DIR__ . "/config.php";
session_start();

$login = '';


if (isset($_POST['inputEmail'])) {
    $login = $_POST['inputEmail'];
}
if(empty($login) or empty($_POST['inputEmail'])){
    header('Location: /login.php');

}else{
    try {
        $dbh = new PDO($dsn);
        $dbh->setAttribute(PDO::ATTR_ERRMODE , PDO::ERRMODE_EXCEPTION );
        $statement = $dbh->prepare("SELECT * FROM users WHERE email = :varLogin");
        $statement->execute(
            [
                'varLogin' => $login,
            ]
        );
        $results = $statement->fetch();
        if(!empty($results and password_verify($_POST['inputPassword'],$results['password']))){
            $_SESSION['user'] = $results;
            $_SESSION['errorLogin'] = false;
            header('Location: /index.php');
        }
        else {
            $_SESSION['errorLogin'] = true;
            $_SESSION['errorConnection'] = "Identifiant ou mot de passe invalide.";
            header('Location: /login.php');
        }
    } catch (PDOException $e) {
        die($e->getMessage());
    }

}



