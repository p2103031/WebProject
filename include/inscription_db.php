<?php include_once __DIR__ . "/config.php";
session_start();

$firstname = "";
$name = "";
$email = "";


if (isset($_POST['myfirstName'])) {
    $firstname = $_POST['myfirstName'];
}  
if (isset($_POST['myName'])) {
    $name = $_POST['myName'];
}
if (isset($_POST['myEmail'])) {
    if (filter_var($_POST['myEmail'], FILTER_VALIDATE_EMAIL)) {
        $email = $_POST['myEmail'];
    }
}
if(empty($firstname) or empty($name) or empty($_POST['myPassword']) or empty($email)){
    header('Location: /inscription.php');

}else{
    try {
        $dbh = new PDO($dsn);
        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $statement = $dbh->prepare("SELECT email FROM users WHERE email=:varMail");
        $statement->execute([
            'varMail' => $email
        ]);
        $results = $statement->fetch();
        if(!empty($results)){
            $_SESSION['errorSubscription'] = "Adresse mail déja utilisée !";
            header('Location: /inscription.php');
    
        }else{
            $new_pass = password_hash($_POST['myPassword'],PASSWORD_DEFAULT);
            $statement = $dbh->prepare("INSERT INTO users(email,name,lastname,password,updated) VALUES(:varEmail,:varName,:varFirstName,:varPassword,datetime());");
            $statement->execute(
                [
                    'varFirstName' => $firstname,
                    'varName' => $name,
                    'varEmail' => $email,
                    'varPassword' => $new_pass
                ]
            );
            $results = $statement->fetch();
            if ($results === false) {
                header('Location: /index.php');
            }  
        }
    } catch (PDOException $e) {
        die($e->getMessage());
    }
}



