<?php include_once __DIR__ . "/config.php";
session_start();

$firstNameContact="";
$lastNameContact="";
$emailContact="";
$messageContact="";

if (isset($_POST['firstNameContact'])){
    $firstNameContact = $_POST['firstNameContact'];
}
if (isset($_POST['lastNameContact'])){
    $lastNameContact = $_POST['lastNameContact'];
}
if (isset($_POST['emailContact'])){
    if (filter_var($_POST['emailContact'], FILTER_VALIDATE_EMAIL)) {
        $emailContact = $_POST['emailContact'];
    }
}
if (isset($_POST['contentContact'])){
    if($_POST['contentContact'] != ""){
        $messageContact = $_POST['contentContact'];
    }
}
if(empty($firstNameContact) or empty($lastNameContact) or empty($emailContact) or empty($messageContact)){
    header('Location: /contact.php');

}else{
    try{
        $dbh = new PDO($dsn);
        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $statement = $dbh->prepare("INSERT INTO contact(email,name,lastname,message,date_) VALUES(:varMail,:varName,:varLastname,:varMessage,datetime());");
        $statement->execute([
            'varMail' => $emailContact,
            'varName' => $firstNameContact,
            'varLastname' => $lastNameContact,
            'varMessage' => $messageContact
        ]);
        $result = $statement->fetch();
        header('Location: /index.php');
        
        
    }catch(PDOException $e){
        die($e->getMessage());
    
    }

}
