<?php
function getActiveURL($fileName)
{
    if ($_SERVER['PHP_SELF'] == $fileName) {
        return ('active');
    }
}

function getImgFormat()
{
    if (preg_match('/avif/', $_SERVER['HTTP_ACCEPT'])) {
        return '.avif';
    }
    if (preg_match('/webp/', $_SERVER['HTTP_ACCEPT'])) {
        return '.webp';
    }
    return '.jpg';
}

function getHobbies()
{
    if (isset($_POST['myHobby'])) {
        $_hobbie = $_POST['myHobby'];
        if (empty($_hobbie)) {
            echo ("You didn't select any hobbies.");
        } else {
            $N = count($_hobbie);

            echo ("You selected $N hobbie(s): ");
            for ($i = 0; $i < $N; $i++) {
                echo ($_hobbie[$i]);
                if ($i != $N - 1) {
                    echo (",");
                }
            }
        }
    }
}

function getFile()
{
    if (!empty($_FILES['myAvatar']['name'])) {
        $target_dir = "uploads/";
        $target_file = $target_dir . basename($_FILES["myAvatar"]["name"]);
        $uploadOk = 1;
        $imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION)); //Check if it's a real image
        $check = getimagesize($_FILES["myAvatar"]["tmp_name"]);
        if ($check !== false) {
            $uploadOk = 1;
        } else {
            $uploadOk = 0;
        }
        if ($uploadOk == 1) {
            move_uploaded_file($_FILES["myAvatar"]["tmp_name"], $target_file);
            echo ('<img src="' . $target_file . '" alt="File upload">');
        }
    }
}

function connected()
{
    if (!empty($_SESSION['user'])) {
            echo ('<a class="nav-link" href="/">'.$_SESSION['user']['lastname'].'</a></li>
                <li class="nav-item">
            <a class="nav-link" href="/include/logout.php">Logout</a>');
        }
    else {
        echo ('<a class="nav-link" href="/login.php">Login</a>');
    }
}

function inputError($error_type){
    switch($error_type){
        case 'subscription':
            if(!empty($_SESSION['errorSubscription'])){
                echo('<p class=rouge>'.$_SESSION['errorSubscription'].'</p>');
                unset($_SESSION['errorSubscription']);
                break;
            }
            break;
        case 'connection' :
            if(!empty($_SESSION['errorConnection'])){
                echo('<p class=rouge>'.$_SESSION['errorConnection'].'</p>');
                unset($_SESSION['errorConnection']);
                break;
            }
            break;    
    }
}

function connexionCookie()
{
    if (!empty($_COOKIE["user"])) {
        $tab = unserialize($_COOKIE["user"]);
        if ($tab[0] == "admin@gmail.com" & $tab[1] == "admin") {
            $_SESSION['admin'] = true;
        }
    }
}


# Constante pour format d'image
define('FORMAT', getImgFormat());

# Constantes php utilisées dans le bottom.php
const NUM = '04.74.35.45.62';
const MAIL = 'jules.valette2@etu.univ-lyon1.fr';
const ADRESS = '9 Rue Pierre de Coubertin';
const ADRESS_COMP = '01250 Ceyzériat';

# Variables navbar.php
$logoTitle = 'JulesValette';
$delim = '.io';
$homePage = 'Home';
$contactPage = 'Contact';

# Variables header.php
$pageTitle = 'Jules Valette';
$relCSS = '/css/style.css';

# Variables config db 
$dsn = "sqlite:".__DIR__."/../database/database.db";


