<?php session_start()?>
<!doctype html>
<html lang="en">
    <head>
        <?php include  'modules/head.php'; ?>
    </head>
    <body>
        <?php include  'modules/navbar.php'; ?> 
        <table>
            <thead>
                <tr>
                    <th colspan="2">Infos Récupérées</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>Prénom :</td>
                    <td><?php if(isset ($_POST['myfirstName'])) echo($_POST['myfirstName'])?></td>
                </tr>
                <tr>
                    <td>Nom :</td>
                    <td><?php if(isset ($_POST['myName'])) echo($_POST['myName'])?></td>
                </tr>
                <tr>
                    <td>Mail :</td>
                    <td><?php if(isset ($_POST['myEmail'])) echo($_POST['myEmail'])?></td>
                </tr>
                <tr>
                    <td>Passions :</td>
                    <td>
                        <?php getHobbies()?>
                    </td>
                </tr>
                <tr>
                    <td>Photo :</td>
                    <td>
                        <?php getFile() ?>
                    </td>
                </tr>
            </tbody>
        </table>
        <?php include  'modules/footer.php'; ?>
    </body>
</html>